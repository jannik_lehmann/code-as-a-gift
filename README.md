# HOHOHO!
![image](https://gitlab.com/jannik_lehmann/code-as-a-gift/-/raw/master/screenshot.png)
## How to install:

- git clone
- npm install

## How to run:

`node hoho.js $YOURFILEPATH`

## How to activate silly mode:

`node hoho.js $YOURFILEPATH silly`
